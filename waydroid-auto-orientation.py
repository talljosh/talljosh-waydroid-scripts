#!/usr/bin/python3
'''
waydroid-auto-orientation.py - rotate waydroid screen based on iio-sensor-proxy


MIT License

Copyright (c) 2022 Joshua D. Bartlett

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


Usage information

This script watches the DBus interface provided by iio-sensor-proxy, then uses
adb to set the screen orientation of the Waydroid session to match. It is most
useful if you don't care about having the screen of your host OS rotate when
you rotate your device, but you do care about your Android session rotating.

This script requires the iio-sensor-proxy, python3-dbus, and adb packages to be
installed. If you don't already have them, you can install them on Ubuntu with:

    $ sudo apt install iio-sensor-proxy python3-dbus

This script relies on adb being able to talk to your Waydroid session. See
https://docs.waydro.id/faq/using-adb-with-waydroid for information. To save you
having to run "adb connect" manually, you can pass your device IP address to
this script using the --waydroid-ip= command-line argument. This is generally
a good idea, as this script will tell adb to reconnect, e.g., if the Waydroid
session gets restarted.

If you run this script and your Waydroid screen is rotated to something other
than the correct orientation, you can use the --offset= argument to tweak which
way is up.

It can be useful to put this script in your user's start-up applications on
your host OS. In this situation, the --logfile= argument can be helpful for
troubleshooting any problems.

This script tries to respect the orientation lock toggle within Android, but if
you have the Android screen orientation locked, then set it back to auto
rotation, the script won't notice until you rotate the device.

'''

import argparse
import logging
import os
import subprocess
import shutil

import dbus
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib

# This must happen very early on
DBusGMainLoop(set_as_default=True)

log = logging.getLogger(__file__)

parser = argparse.ArgumentParser(
    prog='waydroid-auto-orientation.py',
    description='Subscribes to accelerometer changes over DBus, and uses adb to rotate the waydroid orientation',
)
parser.add_argument('-l', '--logfile')
parser.add_argument(
    '-i', '--waydroid-ip',
    help='If this argument is given, the script will tell adb to connect to this IP address',
)
parser.add_argument(
    '-o', '--offset', type=int, default=0,
    help=(
        'A numerical offset to add to the detected orientation before passing it on to adb. For example, '
        'if the sensors say the display is at "normal" orientation when you consider the left-hand side '
        'of the screen to be upward, pass in an offset of -1 (or 3).'),
)


class ScreenRotationManager:
    PROPS_IFACE = 'org.freedesktop.DBus.Properties'
    SENSOR_IFACE = 'net.hadess.SensorProxy'

    ORIENTATIONS = {
        'normal': 0,
        'right-up': 1,
        'bottom-up': 2,
        'left-up': 3,
    }

    def __init__(self, *, system_bus=None, offset=0, ip_addr=None):
        if system_bus is None:
            system_bus = dbus.SystemBus()
        self.orientation_offset = offset
        if ip_addr and ':' not in ip_addr:
            ip_addr += ':5555'
        self.ip_addr = ip_addr

        self.sensor = system_bus.get_object('net.hadess.SensorProxy', '/net/hadess/SensorProxy')
        self.sensor_props = dbus.Interface(self.sensor, self.PROPS_IFACE)

        if not self.sensor_props.Get(self.SENSOR_IFACE, 'HasAccelerometer'):
            raise RuntimeError('No accelerometer detected')

        self.sensor_props.connect_to_signal('PropertiesChanged', self.sensor_props_changed)
        self.sensor.ClaimAccelerometer(dbus_interface=self.SENSOR_IFACE)

    def get_orientation(self):
        return self.sensor_props.Get(self.SENSOR_IFACE, 'AccelerometerOrientation')

    def sensor_props_changed(self, iface_name, changed_props, invalidated_props):
        if 'AccelerometerOrientation' in changed_props:
            self.apply_current_orientation()

    def apply_current_orientation(self):
        orientation = self.get_orientation()
        log.info(f'Orientation: {orientation}')

        if self.ip_addr:
            log.info(f'Ensuring adb is connected to {self.ip_addr}')
            run_adb(['connect', self.ip_addr])

        adb_result = run_adb(['shell', 'settings', 'get', 'system', 'accelerometer_rotation'])
        if adb_result.returncode != 0:
            log.warning('Could not read Android rotation lock setting')
        elif adb_result.stdout.strip() == b'0':
            log.info('Android auto-rotation is switched off. Not setting screen rotation.')
            return

        numerical = (self.ORIENTATIONS[orientation] + self.orientation_offset) % 4
        log.info(f'Setting screen rotation to {numerical}')
        run_adb(['shell', 'wm', 'set-user-rotation', 'lock', str(numerical)])
        run_adb(['shell', 'wm', 'set-user-rotation', 'free'])


def run_adb(args):
    result = subprocess.run([shutil.which('adb')] + args, capture_output=True)

    stdout = result.stdout.decode().strip()
    if stdout:
        log.info(f'adb: {stdout}')

    stderr = result.stderr.decode().strip()
    if stderr:
        log.warning(f'adb: {stderr}')
    return result


def main():
    args = parser.parse_args()
    logging.basicConfig(
        filename=os.path.expanduser(args.logfile) if args.logfile else '/dev/stdout',
        level=logging.INFO,
        format='%(asctime)s:%(levelname)s: %(message)s',
    )

    screen_rotation_manager = ScreenRotationManager(offset=args.offset, ip_addr=args.waydroid_ip)
    screen_rotation_manager.apply_current_orientation()

    loop = GLib.MainLoop()
    try:
        loop.run()
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()
